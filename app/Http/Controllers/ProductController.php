<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Product;
use Illuminate\Support\Facades\Hash;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $id = Auth::id();
        $user = User::find($id); 
        /*if($user == null){
            return view('welcome');}*/
        return view('products.index',['products' => $products]);     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      /*  $id = Auth::id();
        $user = User::find($id); 
       /* if($user == null){
            return view('welcome');
        }*/
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $products = new Product();
       $id = Auth::id();
       $products->name = $request->name;
       $products->price = $request->price;
       $products->user_id = $id;
       $products->save();
      
       return redirect('products');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       // $user_id = Auth::id();
        $products = Product::find($id);
    /*
        if (Gate::denies('owner')){
            if ($user_id != $products->user_id){
                abort(403,"you are not authorized to edit this product");
            }
        }
        */
        return view('products.edit',['products'=>$products]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $products = Product::find($id);
        $products->update($request->except(['_token']));
        return redirect('products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $products = Product::find($id);
       /* if (Gate::denies('manager')){
            abort(403,"you are not authorized to delete products");
        }*/
        $products->delete();
        return redirect('products');
    }
}
