<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function user(){//קשר יחיד לרבים
        return $this->belongsTo('App\User');
    }
    protected $fillable = ['name','user_id','price','status'];
    //protected $fillable = ['name','email','phone'];
}
