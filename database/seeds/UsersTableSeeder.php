<?php

use Illuminate\Database\Seeder;
/*use Illuminate\Support\Facades\Hash;*/

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()//למלא משתמשים בטבלה
    {
        DB::table('users')->insert([

            [
                'name' => 'a',
                'email' => "a@a.com",
                'password' =>Hash::make(12345678),
                'created_at' => date('Y-m-d G:i:s'),
                'role' => 'owner'
            ],
            [
                'name' => 'b',
                'email' => "b@b.com",
                'password' =>Hash::make(12345678),
                'created_at' => date('Y-m-d G:i:s'),
                'role' => 'owner'
            ],
        ]);
    }
}
