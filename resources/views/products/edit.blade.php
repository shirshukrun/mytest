<!--@extends('layouts.app')
@section('content')

<h1>Edit product</h1>
<form method = 'post' action = "{{action('ProductController@update', $product->id)}}" >
@csrf    
@method('PATCH')    
<div class = "form-group">    
    <label for = "name">new name </label>
    <input type = "text" class = "form-control" name = "name" value = "{{$product->name}}">

    <label for = "price">new price </label>
    <input type = "text" class = "form-control" name = "price" value = "{{$product->email}}">

</div>

<div class = "form-group">    
    <input type = "submit" class = "form-control" name = "submit" value = "Update">
</div>

</form>

@endsection-->

@extends('layouts.app')
@section('content')

<h1>Edit a customer</h1>
<form method = 'post' action = "{{action('CustomerController@update', $customer->id)}}" >
@csrf    
@method('PATCH')    
<div class = "form-group">    
    <label for = "name">name customer to update </label>
    <input type = "text" class = "form-control" name = "name" value = "{{$customer->name}}">

    <label for = "email">email customer to update </label>
    <input type = "text" class = "form-control" name = "email" value = "{{$customer->email}}">

    <label for = "phone">phone customer to update </label>
    <input type = "text" class = "form-control" name = "phone" value = "{{$customer->phone}}">
</div>

<div class = "form-group">    
    <input type = "submit" class = "form-control" name = "submit" value = "Update">
</div>

</form>

@endsection

