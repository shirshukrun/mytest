@extends('layouts.app')
@section('content')
@auth
    
   <div class="main">
        <div class="list ">
        
            <h1>Products list</h1>

                <div class="card-body">
                <table class = "table table-hover">
    <tr>
        <th>name</th>
        <th>price</th>
        <th>edit</th>
        <td>delete</td>
       <!-- <th>
            mark as done deal
        </th>-->
        @foreach($products as $product)
    </tr>
    <tr>
        <td>{{ $product->product_name }}</td>
        <td>{{ $product->price }}</td>
        <td>
        @can('owner')
            <a href="{{route('products.edit',$product->id)}}">Edit product</a>
        @endcan
        </td>
        <td>
        @can('owner')
        @method('DELETE')

        <a href="{{route('delete',$product->id)}}">Delete product</a>
        @endcan

        </td>
        
              
        @endforeach
        </tr>
    </table>

        <div class="newProd">
            <a href="{{route('products.create')}}">create a new product</a>  <!--קישור ליצירה-->  
            </div>       
      
   </div>
    
@endauth
@endsection